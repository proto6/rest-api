# Introduction
This project is part of the prototypes group, a set of projects with the goal of practicing different client frameworks such as React and Svelte.
This API is built to be as barebones as possible while still incorporating many of the patterns that would be expected of a production API.

This is the first draft of the readme, will be expanded upon at a later date.

[[_TOC_]]

## Installation
##### Requirements
- Git
- NPM

##### Steps
1. Download this repo
2. Copy the `.env-example` and rename the copy to `.env`
3. Modify the `.env` file as follow:
    - set PORT to a number that represents a open port to run the API on
    - set SECRET to any string, this is used by express-session to sign the session ID cookie. [Read more](https://github.com/expressjs/session#secret)
4. Run `npm install` in the project root
5. Start the project with `npm run start`

# Design
The primary philosophy is simplicity, sometimes at the expense of security or performance, this is primarily due to the nature of this project;
it is not meant to be deployed anywhere but run locally so most of these issues are mitigated.
I hope to document here anytime there is important considerations associated with a design decision that has been discarded for simplicity.

## Considerations

### On overfetching and underfetching
There are two general HTTP `GET`s a client can do; a specific resource, or a collection of resources.
All request for a specific resource must include the resource ID as part of the url and the resources full data should be returned.

When it comes for requests for a collection there seems to be three general solutions;
- minimal
- full
- partial

##### Minimal
Returns the bare minimum for the client to be able to request more information per resource.

**Pros:** Fast, sometimes might be enough, no assumptions on what the client needs

**Cons:** Usually is not enough data and is prone to waterfall requests, underfetching

##### Full
Sidesteps the need for the client to get more data per resource and assumes they would get it anyway.

**Pros:** No waterfall, simple

**Cons:** Slow, might block rendering by fetching data that is not even going to be rendered, overfetching

##### Partial
The more pragmatic solution would be to return only the data needed for rendering the collection, saving the full data for whenever that would be needed.

**Pros:** If configured correctly this approach mostly solves the under/overfetching problem

**Cons:** What fields are needed to render a collection is very prone to change, the full data is probably going to be needed sometime so we're going to fetch some fields multiple times

#### Conclusion
At time of writing the full solution is implimented due to being the easiest, in the future I might swap this over for minimal. Partial rendering is currently not considered due to it solving an inherent problem with REST APIs, if we're looking for a true solution to under/overfetching then that would probably be something closer to GraphQL.

With this project I want to implement the strenghts of REST, but also its weaknesses to some degree.

## Models
The data models are implemented as javascript classes with the intention of encapsulating as much logic as possible instead of having that done by the routes.

### Resource
A generic model to be extended with more specificity by any actual resource.

| Field | Description |
| ------ | ------ |
| id | The resource's unique id. |
| createdAt | A JavaScript date for when this resource was created. |
| updatedAt | A JavaScript date for when this resource was last updated. |

### User
Extends the Resource model with the following fields:

| Field | Description |
| ------ | ------ |
| username | A string used for logging in. |
| fname | Users first name. |
| lname | Users last name. |

### Article
Extends the Resource model with the following fields:

| Field | Description |
| ------ | ------ |
| title | The article's title. |
| text | The article's text. |
| userId | The unique id of the author. |

### Comment
Extends the Resource model with the following fields:

| Field | Description |
| ------ | ------ |
| text | The comment's text. |
| userId | The unique id of the author. |
| articleId | The unique id of the article this comment was left on. |

### Collection
Holds a specific type of resources with methods for interaction.
Note: Any resource can only exist as part of a collection.

## Routes
todo: further documentation
