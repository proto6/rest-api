import { Router } from 'express'
import { Article } from '../models/index.js'

const router = Router()

router
  .route('/')
  .get(function (req, res) {
    return res.send(req.context.articles.sorted)
  })
  .post(function (req, res) {
    if (!req.session.user) {
      return res.sendStatus(401)
    } else if (!req.body.text || !req.body.title) {
      res.sendStatus(400)
    } else {
      const article = new Article(
        req.session.user.id,
        req.body.title,
        req.body.text
      )
      req.context.articles.add(article)
      return res.send(article)
    }
  })

router.param('articleId', function (req, res, next, id) {
  req.article = req.context.articles.findById(id)
  next()
})

router
  .route('/:articleId')
  .get(function (req, res) {
    if (!req.article) {
      return res.sendStatus(404)
    } else {
      return res.send(req.article)
    }
  })
  .put(function (req, res) {
    if (!req.body.text || !req.body.title) {
      res.sendStatus(400)
    } else if (!req.session.user) {
      return res.sendStatus(401)
    } else if (!req.article) {
      return res.sendStatus(404)
    } else if (req.article.userId !== req.session.user.id) {
      return res.sendStatus(403)
    } else {
      const article = req.context.articles.update(req.article.id, {
        text: req.body.text,
        title: req.body.title,
      })
      return res.send(article)
    }
  })
  .delete(function (req, res) {
    if (!req.session.user) {
      return res.sendStatus(401)
    } else if (!req.article) {
      return res.sendStatus(404)
    } else if (req.article.userId !== req.session.user.id) {
      return res.sendStatus(403)
    } else {
      const article = req.context.articles.deleteById(req.article.id)
      return res.send(article)
    }
  })

export default router
