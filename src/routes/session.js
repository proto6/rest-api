import { Router } from 'express'

const router = Router()

router.get('/', function (req, res) {
  if (req.session.user) {
    return res.send(req.session.user)
  } else {
    return res.sendStatus(204)
  }
})

router.post('/login', function (req, res) {
  if (req.session.user) {
    return res.send(req.session.user)
  }
  if (!req.body.username) {
    return res.sendStatus(400)
  }
  var user = req.context.users.find(
    (user) => user.username === req.body.username
  )
  if (!user) {
    return res.sendStatus(404)
  } else {
    req.session.user = user
    return res.send(user)
  }
})

router.get('/logout', function (req, res) {
  if (req.session.user) {
    req.session.destroy()
    return res.sendStatus(204)
  } else {
    return res.sendStatus(404)
  }
})

export default router
