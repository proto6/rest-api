import { Router } from 'express'
import { User } from '../models/index.js'

const router = Router()

router
  .route('/')
  .get(function (req, res) {
    return res.send(req.context.users.data)
  })
  .post(function (req, res) {
    if (!req.body.username || !req.body.fname || !req.body.lname) {
      return res.sendStatus(400)
    }
    if (req.context.users.find((user) => user.username === req.body.username)) {
      return res.sendStatus(409)
    }
    const user = new User(req.body.username, req.body.fname, req.body.lname)
    req.context.users.add(user)
    if (!req.session.user) {
      req.session.user = user
    }
    return res.send(user)
  })

router.param('userId', function (req, res, next, id) {
  req.user = req.context.users.findById(id)
  next()
})

router
  .route('/:userId')
  .get(function (req, res) {
    if (!req.user) {
      return res.sendStatus(404)
    } else {
      return res.send(req.user)
    }
  })
  .put(function (req, res) {
    return res.sendStatus(501)
  })
  .delete(function (req, res) {
    return res.sendStatus(501)
  })

router.get('/:userId/articles', function (req, res) {
  if (!req.user) {
    return res.sendStatus(404)
  } else {
    return res.send(
      req.context.articles.sorted.filter(({ userId }) => userId === req.user.id)
    )
  }
})

export default router
