import faker from 'faker'
import { v4 as uuidv4 } from 'uuid'
import { cleanString } from '../lib.js'

const seedUsers = 2
const seedArticles = 3

class Resource {
  id
  createdAt
  updatedAt = null

  constructor(createdAt) {
    this.id = uuidv4()
    this.createdAt = createdAt || new Date()
  }

  update() {
    this.updatedAt = new Date()
  }
}

export class User extends Resource {
  username
  fname
  lname

  constructor(username, fname, lname, ...props) {
    super(...props)
    this.username = username
    this.fname = fname
    this.lname = lname
  }

  update({ username, fname, lname }) {
    super.update()
    this.username = username || this.username
    this.fname = fname || this.fname
    this.lname = lname || this.lname
  }
}

export class Article extends Resource {
  userId
  title
  text

  constructor(userId, title, text, ...props) {
    super(...props)
    this.userId = userId
    this.title = title
    this.text = text
  }

  update({ title, text }) {
    super.update()
    this.title = title || this.title
    this.text = text || this.text
  }
}

export class Collection {
  data = []

  constructor(data) {
    this.data = data || []
  }

  find(fn) {
    return this.data.find(fn)
  }

  filter(fn) {
    return this.data.filter(fn)
  }

  findById(id) {
    return this.data.find(({ id: resourceId }) => resourceId === id)
  }

  add(resource) {
    this.data = [...this.data, resource]
    return this.data
  }

  update(id, values) {
    let resource = this.findById(id)
    resource.update(values)
    return resource
  }

  delete(fn) {
    this.data = this.data.filter(fn)
    return this.data
  }

  deleteById(id) {
    this.data = this.data.filter(({ id: resourceId }) => resourceId !== id)
    return this.data
  }

  get length() {
    return this.data.length
  }

  get sorted() {
    return this.data.sort((a, b) => {
      return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
    })
  }
}

const users = new Collection()
const articles = new Collection()

if (seedUsers) {
  for (let i = 0; i < seedUsers; i++) {
    users.add(
      new User(
        `user${i + 1}`,
        faker.name.firstName(),
        faker.name.lastName(),
        faker.date.recent()
      )
    )
  }
}

if (seedArticles) {
  for (const user of users.data) {
    for (let i = 0; i < seedArticles; i++) {
      articles.add(
        new Article(
          user.id,
          faker.lorem.sentence(),
          cleanString(faker.lorem.paragraphs(5)),
          faker.date.recent()
        )
      )
    }
  }
}

export default {
  users,
  articles,
}
