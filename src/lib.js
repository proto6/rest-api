export function cleanString(string) {
  return string
    .replace(/\r/g, '\n')
    .replace(/(?<=\s) +/g, '')
    .trim()
}
