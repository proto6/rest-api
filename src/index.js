import express from 'express'
import session from 'express-session'
import cors from 'cors'
import models from './models/index.js'
import routes from './routes/index.js'

const app = express()

// Middlewares
app.use(
  cors({
    origin: ['http://localhost:3000', 'http://localhost:5000'],
    credentials: true,
  })
)
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
  })
)
app.use(function (req, res, next) {
  // console.log(
  //   `recieved a ${req.method} request at ${req.originalUrl}, with body:`
  // )
  // console.log(req.body)
  req.context = models
  next()
})

// Routes
app.use('/session', routes.session)
app.use('/users', routes.user)
app.use('/articles', routes.article)

app.listen(process.env.PORT, () =>
  console.log(`Example app listening on port ${process.env.PORT}`)
)
